#coding: utf-8
__author__ = 'yumere'

from flask import Flask, request, render_template, redirect, url_for, g, jsonify
import pymysql
import json
import datetime
import base64
import argparse

from config import baseDIR
from config import DB_NAME, DB_USER, DB_PW

import modules.engine.EngineMain as engine

image_path = "/image_dir/"
image_dir = baseDIR+image_path

app = Flask(__name__)
app.secret_key = "Capstone"
app.debug = True

@app.before_request
def db_connect():
    g.db_conn = pymysql.Connect(user=DB_USER, passwd=DB_PW, db=DB_NAME, use_unicode=True, charset='utf8')
    g.db_cursor = g.db_conn.cursor()

@app.teardown_request
def teardown_request(exception):
    db_cursor = getattr(g, 'db_cursor', None)
    if db_cursor is not None:
        db_cursor.close()

    db_conn = getattr(g, 'db_conn', None)
    if db_conn is not None:
        db_conn.close()

@app.before_first_request
def model_load():
    engine.modelLoad()
    pass

@app.route('/', methods=["GET"])
def index():
    g.db_cursor.execute("select id, user_name from APP_INFO")
    app_id, user_name = g.db_cursor.fetchone()
    print app_id, user_name

    return "<h1>This is Index Page</h1>"

@app.route("/upload", methods=["POST", "GET"])
def upload():
    # GET Test
    if request.method == "GET":
        return render_template("upload_test.html")

    elif request.method == "POST":
        data = json.loads(request.data)

        try:
            app_id = data['app_id']
            g.db_cursor.execute("select count(*) from APP_INFO where app_id=%s", (app_id,))
            count = g.db_cursor.fetchone()[0]

            # 사용자가 존재하지 않을 경우
            if count == 0:
                g.db_cursor.execute("insert into APP_INFO(app_id, created_at, updated_at) values(%s, %s, %s)", (app_id, datetime.datetime.now(), datetime.datetime.now()))
                g.db_conn.commit()

            g.db_cursor.execute("select id, user_name from APP_INFO where app_id=%s",
                (app_id,))
            app_p_id, user_name = g.db_cursor.fetchone()

        except Exception, e:
            return jsonify({'error_msg': 'app_id error, ' + str(e)})

        try:
            image_name = data['image_name']

        except Exception, e:
            return jsonify({'error_msg': 'image_name error, '+str(e)})

        try:
            hash_id = data['hash_id']
        except Exception, e:
            return jsonify({'error_msg': 'hash_id error, '+str(e)})

        try:
            file = base64.b64decode(data['file'])
        except Exception, e:
            return jsonify({'error_msg': 'file error, '+str(e)})

        with open(image_dir+image_name, "wb") as f:
            f.write(file)

        # 클라이언트 태그 분류
        tag_list = data['tag_list']
        analyzed_tags, added_tags = [i.split(",") for i in tag_list.split("|")]

        # 태그 추출
        analyzed_tags = engine.extractTags(image_dir+image_name)
        tag_list = "|".join([analyzed_tags, ','.join(added_tags)])

        try:
            g.db_cursor.execute("insert into PHOTO_INFO(img_name, img_url, hash_id, tag_list, created_at, updated_at, app_info_id)"
                               "values(%s, %s, %s, %s, %s, %s, %s) on duplicate key update tag_list=%s",
            (image_name, image_path, hash_id, tag_list, str(datetime.datetime.now()), str(datetime.datetime.now()), app_p_id, tag_list))
            g.db_conn.commit()
        except Exception, e:
            print e
            print "DB Insert Error"
            return redirect(url_for("upload"))

        return_data = {
            'app_id': app_id,
            'hash_id': hash_id,
            'tag_list': tag_list,
            'error_msg': 'success'
        }

        return jsonify(return_data)

@app.route('/tmp', methods=["POST", "GET"])
def tmp():
    if request.method == "GET":
        return render_template("upload_test.html")

    elif request.method == "POST":
        pass

@app.route('/sync', methods=["POST"])
def sync():
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-p", "--port", type=int, help="Run at this port")
    options = parser.parse_args()

    port = 5000 if options.port is None else options.port

    app.run(host="0.0.0.0", port=port)
