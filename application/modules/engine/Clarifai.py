#coding: utf-8
__author__ = 'yumere'

import requests
import json
import argparse
from PIL import Image
import imghdr

from config import ACCESS_TOKEN

def get_tag(image_dir):

    data = open(image_dir, "rb").read()

    files = {
        'encoded_data': data
    }
    headers = {
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    r = requests.post("https://api.clarifai.com/v1/tag/", files=files, headers=headers)
    result_data = json.loads(r.content)

    if result_data['status_code'] != u"OK":
        print result_data
        return False, False
    if result_data['results'][0]['status_code'] != u"OK":
        print result_data
        return False, False

    tags = result_data['results'][0]['result']['tag']['classes']
    probs = result_data['results'][0]['result']['tag']['probs']

    #return (map(lambda x: str(x), tags), probs)
    return tags[:5]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', help='input file')

    options = parser.parse_args()

    #if options.file is None:
    #    parser.print_help()
    #    sys.exit(-1)

    tag_data = get_tag("../../image_dir/20150526_211327_1.jpg")
    print tag_data
