#coding: utf-8
import requests
import json
import argparse
import sys

api_key = "5a839298ba6d49d26e8093421c3e3de7"

def get_api_status():
    url = "http://api.newocr.com/v1/key/status?key="+api_key
    json_data = json.loads(requests.get(url).content)
    # print requests.get(url).content

    print json_data

def upload_file(file_descriptor):
    files = {
        'file': file_descriptor.read()
    }

    r = requests.post("http://api.newocr.com/v1/upload?key="+api_key, files=files)
    json_data = json.loads(r.content)

    if json_data['status'] == "success":
        return json_data['data']['file_id'], json_data['data']['pages']

    else:
        return False, False

def run_ocr(file_id, pages, lang="kor"):
    """
    lang:
    kor = Korean
    eng = English
    jpn = Japanese

    """

    url = "http://api.newocr.com/v1/ocr?key=%s&file_id=%s&page=%s&lang=%s&psm=3" % (api_key, str(file_id), str(pages), lang)
    json_data = json.loads(requests.get(url).content)

    if json_data['status'] == "success":
        return json_data['data']['text']
    else:
        return False

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', help="input file", type=argparse.FileType("rb"))

    options = parser.parse_args()

    if options.file is None:
        parser.print_help()
        sys.exit(-1)

    get_api_status()    
    file_id, pages = upload_file(options.file)

    # print run_ocr(file_id, pages)
    text = run_ocr("9a469456241d9701dc18625317a5274a", "1")
    print text

