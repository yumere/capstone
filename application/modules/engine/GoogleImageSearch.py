__author__ = 'kaien'

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
import sys
import os.path
#  Function to search file from local machine
def searchfile(imgName):

    #  Assigning the user agent string for PhantomJS
    dcap = dict(webdriver.DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:29.0) Gecko/20100101 Firefox/29.0")

    if os.path.isfile(imgName):
        pass
    else:
        sys.exit()

    try:
        browser = webdriver.PhantomJS(desired_capabilities=dcap)

        browser.implicitly_wait(5)

        browser.get('http://www.google.com.au/imghp')

        # Click "Search by image" icon
        elem = browser.find_element_by_class_name('gsst_a')
        elem.click()

        # Switch from "Paste image URL" to "Upload an image"
        browser.execute_script("google.qb.ti(true);return false")

        # Set the path of the local file and submit
        elem = browser.find_element_by_id("qbfile")

        elem.send_keys(imgName)

        #  Clicking 'Visually Similar Images'
        imgTag = browser.find_element_by_class_name("_gUb").text

        browser.quit()
    except Exception:
        return None

    return imgTag

#print searchfile("/Users/kaien/Desktop/test5.png")