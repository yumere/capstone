#-*- coding: utf-8 -*-
import sys
__author__ = 'kaien'

import os
import cv2
import pickle
import numpy as np
import json
import time

import Clarifai
import TranslateENtoKR
import GoogleImageSearch

# des, response for KNN.train
FINAL_DES = list()
FEATURE_RESPONSE = list()

# Train model
FILE_LIST = ["airplane", "butterfly", "car", "dog", "hamburger", "keyboard", "laptop", "monitor", "mouse", "watch"]

# This is Union-Find algoirithm to connect key points that are close each other, using weighted quick union by rank path compresstion.
class UF:

    def __init__(self, N):
        self._id = list(range(N))
        self._count = N
        self._rank = [0] * N

    def find(self, p):
        id = self._id
        while p != id[p]:
            id[p] = id[id[p]]   # Path compression using halving.
            p = id[p]
        return p

    def count(self):
        return self._count

    def connected(self, p, q):
        return self.find(p) == self.find(q)

    def union(self, p, q):
        id = self._id
        rank = self._rank

        i = self.find(p)
        j = self.find(q)
        if i == j:
            return

        self._count -= 1
        if rank[i] < rank[j]:
            id[i] = j
        elif rank[i] > rank[j]:
            id[j] = i
        else:
            id[j] = i
            rank[i] += 1

    def __str__(self):
        return " ".join([str(x) for x in self._id])

    def __repr__(self):
        return "UF(" + str(self) + ")"

# to check time taken
def checkTime(func):
    def newFunc(*args, **kwargs):
        start = time.time()
        func(*args, **kwargs)
        end = time.time()
        print "%s : %s - %s = %s" % (func.__name__ + " Function", time.localtime(end).tm_sec, time.localtime(start).tm_sec, str(end-start) +"sec")
    return newFunc

# to classfy a image
def classifyImage(template, des, response):

    knn = cv2.KNearest()
    knn.train(des,response)

    tmpKeys,tmpDes = extractFeatures(template)

    if tmpKeys < 1:
        return 0
    matchScore = list()

    for h, tmpDesc in enumerate(tmpDes):
        tmpDesc = np.array(tmpDesc,np.float32).reshape((1,128))
        retval, results, neigh_resp, dists = knn.find_nearest(tmpDesc,5)
        matchScore.append(retval)

    return calculateMatchingRate(matchScore)

# to calculate matching rate
def calculateMatchingRate(scoreList):

    retDict = dict()

    for score in scoreList:
        if score in retDict.keys():
            retDict[score] = retDict[score] + 1
        else:
            retDict.update({score:1})

    sumCount = 0.0

    for key, value in retDict.items():
        sumCount = sumCount + value

    for key, value in retDict.items():
        retDict.update({key : value/sumCount})

    #retList = sorted(retDict.items(), key=lambda x: x[1],reverse=True)
    return retDict

# To extract features in a image
def extractFeatures(npImgIn, threshold=1000):
    # check exception
    if npImgIn.size == 0:
        return 0,0

    surf = cv2.SURF(400)
    surf.hessianThreshold = threshold
    kp, des = surf.detectAndCompute(npImgIn,None)

    if len(kp) < 1:
        return 0, 0
    return kp, des

# to convert to json
def convertToJson(list):
    return json.dumps(list)

# to translate tag names
def translateTagNames(scoreDict,fileList):
    tagDict = dict()
    for key in scoreDict.keys():
        tagDict[fileList[int(key)-1]] = scoreDict[key]

    retList = sorted(tagDict.items(), key=lambda x: x[1],reverse=True)
    return retList

# To remove Image's noise for making more perfectly!!
def removeNoise(npImgIn):
    npImgOut = cv2.fastNlMeansDenoisingColored(npImgIn,None,10,10,7,21)
    return npImgOut

# To convert Grayscale Image
def convertGrayscale(npImgIn):
    npImgOut = cv2.cvtColor(npImgIn,cv2.COLOR_BGR2GRAY)
    return npImgOut

# To trim a image
def im_trim(img,x,y,w,h):

    if w == 0:
        w = 100
    if h == 0:
        h = 50
    img_trim = img[y:y+h, x:x+w] #put the result trimmed
    return img_trim

# To make segments in a image
def segmentalizeImage(npImgIn):

    #trhd = 10000
    removedNoiseImg = removeNoise(npImgIn)
    removedNoiseKp, removedNoiseDes = extractFeatures(removedNoiseImg,5000)

    #while(len(removedNoiseKp) >= 200): # to control threshold
    #    removedNoiseKp, removedNoiseDes = extractFeatures(removedNoiseImg,trhd)
    #    trhd = (trhd / 3) * 2

    cnt = 0

    gp = [[0]*len(removedNoiseKp) for x in range(len(removedNoiseKp))]

    for i, point in enumerate(removedNoiseKp):
        cnt += 1
        for j , pointCompared in enumerate(removedNoiseKp[cnt:]):
            j = j + cnt

            #print i , j

            kpX = point.pt[0] # X-coordinate
            kpY = point.pt[1] # Y-coordinate
            kpR = point.size / 2 # radius

            kpComparedX = pointCompared.pt[0] # X-coordinate
            kpComparedY = pointCompared.pt[1] # Y-coordinate
            kpComparedR = pointCompared.size / 2 # radius
            #print kpX, kpY, kpR, kpComparedX, kpComparedY, kpComparedR

            if (kpX-kpComparedX)**2 + (kpY-kpComparedY)**2 <= (kpR+kpComparedR)**2: # inside circle.
                gp[i][j]= 1
                #print i , j , 1

    a = UF(len(removedNoiseKp))
    for i in xrange(len(removedNoiseKp)):
        for j in xrange(len(removedNoiseKp)):
            if gp[i][j] == 1:
                a.union(i,j)


    gp2 = [[] for i in range(len(removedNoiseKp))]
    for i,j in enumerate(a._id):
        gp2[j].append(i)

    imgList = []
    for i in gp2:
        xMax = 0
        xMin = 99999
        yMax = 0
        yMin = 99999
        if len(i) > 1:
            for j in i:
                if removedNoiseKp[j].pt[0] + removedNoiseKp[j].size / 2 > xMax:
                    xMax = removedNoiseKp[j].pt[0] + removedNoiseKp[j].size / 2
                if removedNoiseKp[j].pt[1] + removedNoiseKp[j].size / 2> yMax:
                    yMax = removedNoiseKp[j].pt[1]+ removedNoiseKp[j].size / 2
                if removedNoiseKp[j].pt[0] - removedNoiseKp[j].size / 2< xMin:
                    xMin = removedNoiseKp[j].pt[0] - removedNoiseKp[j].size
                if removedNoiseKp[j].pt[1] - removedNoiseKp[j].size / 2< yMin:
                    yMin = removedNoiseKp[j].pt[1] - removedNoiseKp[j].size / 2

            imgList.append(im_trim (removedNoiseImg,xMin,yMin,xMax-xMin,yMax-yMin))

    return imgList

# to laod trained image model
@checkTime
def modelLoad():
    global FINAL_DES, FEATURE_RESPONSE
    objDes_list = list()
    response_list = list()

    # merge des
    for i, j in enumerate(FILE_LIST):
        objDes_list.append(pickle.load(open(os.getcwd()+"/modules/engine/TrainedModels/%s.dat" % j, 'r')))
        for ii, jj in enumerate(objDes_list[i]):
            response_list.append(np.array([float(i+1)] * len(jj), dtype=np.float32))

    # descriptors = objDes1 + ... + objDes10
    ImgDescriptors = reduce(lambda x, y: np.append(x, y), objDes_list)

    for i, j in enumerate(ImgDescriptors):
        for ii,jj in enumerate(ImgDescriptors[i]):
            FINAL_DES.append(jj)
    FINAL_DES = np.array(FINAL_DES,dtype=np.float32)
    FEATURE_RESPONSE = reduce(lambda x, y: np.append(x, y), response_list)

# to extract tags
# @checkTime
def extractTags(imgPath):
    global FINAL_DES,FEATURE_RESPONSE

    if os.path.isfile(imgPath):
        pass
    else:
        return None

    finalTagList = list()

    # matching and extracting
    template = cv2.imread(imgPath) # input a image file

    tmpImgList = segmentalizeImage(template)

    for i, eachImg in enumerate(tmpImgList):
        scoreDict = classifyImage(eachImg,FINAL_DES,FEATURE_RESPONSE)
        print scoreDict
        if scoreDict == 0:
            continue
        sortedTagList = translateTagNames(scoreDict,FILE_LIST)

        finalTagList.append((sortedTagList[0][0]))

    # add a tag to finalTagList by using GoogleImageSearch
    finalTagList.append(GoogleImageSearch.searchfile(imgPath))

    # add tags to finalTagList by using Clarifai API
    for i in Clarifai.get_tag(imgPath):
       finalTagList.append(i)

    # remove duplication from set
    finalTagSet = set(finalTagList[:])

    stopList = ["butterfly", "false", "nobody"]


    # Translate from English to Korean
    tags = list()
    for i in finalTagSet:
        if i in stopList:
            continue
        tags.append(TranslateENtoKR.translate(i))

    print tags
    return ",".join(tags)

# main
def engineMain():
    modelLoad()
    print extractTags('ImagesForTraining/watch/240_0073.jpg')

if __name__ == "__main__":
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', help='Input Image File')
    parser.add_argument('-t', '--train', help='Training Model')

    args = parser.parse_args()

    if args.file:
        modelLoad()
        extractTags(args.file)
    if parser.train:
        #MakeTrainModel.makeModel()
    """
    engineMain()
