__author__ = 'kaien'

import os
from os import walk
import cv2
import pickle
import numpy as np

import EngineMain

def __getFileNames(path):
    f = []
    for (dirpath, dirnames, filenames) in walk(path):
        f.extend(filenames)
        break
    return f[1:]

def makeModel(imagePath,tagName):
    keyPoints = []
    descriptors = []
    fileNames = __getFileNames(imagePath)

    # KNN model and training on descriptors
    #responses = []

    #template = cv2.imread('/Users/kaien/Desktop/test.png')
    #keys,desc = ExtractKp.extractFeatures(template)

    cnt = -1
    # Start processing images
    for imageFile in fileNames:
        cnt += 1
        print imageFile , cnt
        # Extract keypoints and description
        try:

            img = cv2.imread(imagePath + "/" + imageFile)
            #img = np.asarray(img[:,:])
            kp, des = EngineMain.extractFeatures(img)

            if len(kp) > 0:
                #responses.append(tagName)
                flatdes = des.astype(np.float32)
                keyPoints.append(kp)
                descriptors.append(flatdes)
            else:
                continue
        except:
            continue

        fileObject = open(os.getcwd()+'/TrainedModels/'+tagName+'.dat','wb')
        pickle.dump(descriptors,fileObject)
        fileObject.close()

        """
        fileObject = open(os.getcwd()+'/TrainedModels/'+tagName+'.dat','r')
        descriptors = pickle.load(fileObject)

        #print type(descriptors)
        #print descriptors
    #np.savetxt(os.getcwd()+'/TrainedModels/'+tagName+'.dat',descriptors,delimiter=" ", fmt="%s")
    #np.loadtxt(os.getcwd()+'/TrainedModels/'+tagName+'.dat')

        #print des
        #print type(des)
        # Convert image as a float type
        #descRow = des.shape[0]
        #descCol = des.shape[1]

        #flatdes = des.reshape(descRow*descCol).astype(np.float32)

    for i, des in enumerate(descriptors):
        #responses = []
        #for a in range(len(des)):
        #    responses.append(tagName)

        samples = np.array(des)
        #responses = np.arange(len(keyPoints[i]),dtype = np.float32)
        responses = np.array([0 for i in range(len(keyPoints[i]))],dtype = np.float32)
        #print responses
        knn = cv2.KNearest()
        knn.train(des,responses)
        #print type(des)

        """

        """
        for h, desc1 in enumerate(desc):
            desc1 = np.array(desc1,np.float32).reshape((1,128))
        retval, results, neigh_resp, dists = knn.find_nearest(desc1,3)
        print retval, results, neigh_resp, dists
        res,dist =  int(results[0][0]),dists[0][0]
        if dist<0.3: # draw matched keypoints in red color
            #print dist, i
            color = (0,0,255)
        else:  # draw unmatched in blue color
            #print dist
            color = (255,0,0)
        """


if __name__ == "__main__":

    #makeModel(os.getcwd()+'/ImagesForTraining/airplane','airplane')
    makeModel(os.getcwd()+'/ImagesForTraining/butterfly','butterfly')
    makeModel(os.getcwd()+'/ImagesForTraining/car','car')
    makeModel(os.getcwd()+'/ImagesForTraining/dog','dog')
    makeModel(os.getcwd()+'/ImagesForTraining/hamburger','hamburger')
    makeModel(os.getcwd()+'/ImagesForTraining/keyboard','keyboard')
    makeModel(os.getcwd()+'/ImagesForTraining/laptop','laptop')
    makeModel(os.getcwd()+'/ImagesForTraining/monitor','monitor')
    makeModel(os.getcwd()+'/ImagesForTraining/mouse','mouse')
    makeModel(os.getcwd()+'/ImagesForTraining/watch','watch')
    #print os.getcwd()+'/ImagesForTraining'
