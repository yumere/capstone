#coding: utf-8
__author__ = 'yumere'

import requests
import json
import argparse
import sys
import base64
import hashlib

if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument('-f', '--file', help="input image file", type=argparse.FileType('r'))
	options = parser.parse_args()

	if options.file is None:
		parser.print_help()
		sys.exit(-1)

	image_data = options.file.read()
	hash_id = hashlib.md5(image_data).hexdigest()
	image_data = base64.b64encode(image_data)

	request_data = {
		'app_id': "00000000-6d70-4297-d4e4-850157d312a1",
	    'image_name': 'test3.png',
	    'hash_id': hash_id,
	    'tag_list': '|',
	    'file': image_data
	}

	#r = requests.post("http://165.246.25.114:61381/upload", data=json.dumps(request_data))
	r = requests.post("http://localhost:5000/upload", data=json.dumps(request_data))
	print r.content
