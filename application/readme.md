# Python Server #
This is Python RESTful API server running on linux Server between Engine and Android Application.

## Technical Stack ##
- Python v2.7.9
- Gunicorn
- Nginx
- flask-RESTful-extend

## Used Library ##
- aniso8601==0.92
- Flask==0.10.1
- Flask-RESTful==0.3.2
- Flask-RESTful-extend==0.3.3
- Flask-SQLAlchemy==2.0
- gunicorn==19.3.0
- itsdangerous==0.24
- Jinja2==2.7.3
- MarkupSafe==0.23
- PyMySQL==0.6.6
- pytz==2015.2
- six==1.9.0
- SQLAlchemy==0.9.9
- Werkzeug==0.10.4

## Database Schema ##
#### APP_INFO ####
    create table APP_INFO(
        id int not null auto_increment,
        app_id varchar(100) not null,
        user_name varchar(100),
        created_at datetime not null,
        updated_at datetime not null,
        primary key(id),
        unique key(app_id)
    )ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8

#### PHOTO_INFO ####
    create table PHOTO_INFO(
        id int not null auto_increment,
        img_name varchar(100) not null,
        img_url varchar(100) not null,
        hash_id varchar(32) not null,
        tag_list text,
        created_at datetime not null,
        updated_at datetime not null,
        app_info_id int not null,
        primary key(id),
        unique key(hash_id),
        foreign key(app_info_id) references APP_INFO(id) 
    )ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;