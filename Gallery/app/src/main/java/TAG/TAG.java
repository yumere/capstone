package TAG;

/**
 * Created by hahahia on 2015-05-01.
 */
public class TAG {
    private int tag_id;
    private int img_id;
    private String tag_name;

    public int getTag_id() { return tag_id; }
    public int getImg_id() { return img_id; }
    public String getTag_name() { return tag_name; }

    public void setTag_id(int tag_id) { this.tag_id = tag_id; }
    public void setImg_id(int img_id) { this.img_id = img_id; }
    public void setTag_name(String tag_name) { this.tag_name = tag_name; }
}