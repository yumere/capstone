package example.hahahia.gallery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by hahahia on 2015-05-17.
 */
public class NewPhotoReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        System.out.println("test1234");
        String selectedImageUri = arg1.getData().getPath();
        Log.d("TAG", "Received new photo:::"+selectedImageUri );
        Log.d("TAG","file Path"+getRealPathFromURI(arg1.getData(),arg0));
    }
    public String getRealPathFromURI(Uri contentUri,Context context)
    {
        try
        {
            String[] proj = {MediaStore.Images.Media.DATA};

            Cursor cursor =  context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        catch (Exception e)
        {
            return contentUri.getPath();
        }
    }
}