package example.hahahia.gallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import Database.DBManager;
import IMG.IMG;
import TAG.TAG;
import Unique.Unique;

public class DetailViewActivity extends ActionBarActivity implements OnClickListener{
    private Context mContext;
    private IMG img; // 특정 이미지의 객체
    private Bitmap bm; // 보여주는 이미지
    private Bitmap sendIMG; // 썸네일화 해서 보내지는 이미지
    private DBManager manager; // DB 관리 클래스
    private String[] view_tag_list;
    private ArrayList<Integer> index_tag_list = new ArrayList<>();
    private ArrayList<TAG> tag_list;
    private int tag_id;
    private String tag_name;

    void setting(){
        setContentView(R.layout.activity_detail_view);
        mContext =  getApplicationContext();
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        manager = new DBManager(getApplicationContext());

        /* index값 받아와서 출력하긔 */
        int index = extras.getInt("img_id");
        img = manager.selectData(index);

        if(img.getTag_list() == null)
            img.setTag_list("|");

        System.out.println("img_id : " + img.getImg_id());
        System.out.println("img_name : " + img.getImg_name());
        System.out.println("img_path : " + img.getImg_path());
        System.out.println("img_tag_list : " + img.getTag_list());
        Unique unique = new Unique();

        tag_list = manager.selectTagList(img);

        ArrayList<String> tmp_list = new ArrayList<>();
        for(int k=0; k<tag_list.size(); k++){
            tmp_list.add(tag_list.get(k).getTag_name());
            index_tag_list.add(k, tag_list.get(k).getTag_id());
        }
        view_tag_list = tmp_list.toArray(new String[tmp_list.size()]);

        /* 보여주기용 이미지 생성  */
        BitmapFactory.Options bfo = new BitmapFactory.Options();
        bfo.inSampleSize = 2;
        ImageView iv = (ImageView)findViewById(R.id.imageView);
        bm = BitmapFactory.decodeFile(img.getImg_path(), bfo);
        iv.setImageBitmap(bm);

        /* 태그 리스트 GridView 생성 */
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new DispClass(this, android.R.layout.simple_list_item_1, view_tag_list));


        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> a, View v, int r, long i) {
                try {
                    AlertDialog.Builder alert = new AlertDialog.Builder(DetailViewActivity.this);
                    tag_id = index_tag_list.get(r);
                    tag_name = view_tag_list[r];
                    System.out.println("select tag name : " + tag_name);
                    alert.setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    String str = img.getTag_list();
                                    String replaceStr;
                                    if(str.contains(tag_name + "|")){
                                        if(str.contains("," + tag_name + "|")){
                                            replaceStr = str.replace("," + tag_name + "|", "|");
                                        }
                                        else {
                                            replaceStr = str.replace(tag_name + "|", "|");
                                        }

                                        img.setTag_list(replaceStr);
                                        manager.updateData(img);
                                        manager.deleteTag_byImg_id(tag_id);
                                    }
                                    else if(str.contains(tag_name + ",")){
                                        replaceStr = str.replace(tag_name+",", "");
                                        img.setTag_list(replaceStr);
                                        manager.updateData(img);
                                        manager.deleteTag_byImg_id(tag_id);

                                    }
                                    else{
                                        manager.deleteTag_byImg_id(tag_id);
                                    }
                                    onResume();
                        }});
                    alert.setNegativeButton("취소",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            onResume();
                        }});
                    alert.setMessage("선택한 태그명을 삭제하시겠습니까?");
                    alert.show();

                } catch (Exception e) {
                    System.out.println(e);
                }
                return true;
            }
        });

        /** 보내는 이미지(resize) 객체 생성*/
        sendIMG = resizeIMG(img.getImg_path());
        /* 이미지 전송을 위해 이미지 -> byte로 변환 */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        sendIMG.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray(); // 이미지 파일 값 설정
        img.setFileData(Base64.encodeToString(imageBytes, Base64.DEFAULT)); // 보내는 이미지 파일 BASE 64
        img.setHash_id(img.getFileData()); // 이미지 파일 hash(MD5)
        // check if you are connected or not
        if(isConnected()){
            System.out.println("connect complete");
        }
        else{
            System.out.println("connect fail");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setting();
    }
    @Override
    public void onResume(){
        super.onResume();
        setting();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_sendToServer) {
            if(isConnected()) {
                new HttpAsyncTask().execute("http://newheart.kr:61381/upload");
            }
            else{
                Toast.makeText(getBaseContext(), "인터넷 연결을 해주세요", Toast.LENGTH_LONG).show();
            }
        }

        else if(id == R.id.action_deleteImage){ // 특정 사진 삭제버튼

            manager.removeData(img); // DB에 있는 사진정보 삭제
            AlertDialog.Builder alert = new AlertDialog.Builder(DetailViewActivity.this);
            alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();      // 삭제 완료 후 메인 Activity로 이동
                }
            });
            alert.setMessage("사진 삭제 완료!!");
            alert.show();
        }

        /* 사용자 태그명 입력 후 추가 */
        else if(id == R.id.action_insertTag){
            AlertDialog.Builder alert = new AlertDialog.Builder(DetailViewActivity.this);
            alert.setMessage("추가할 태그명을 입력하세요!");
            final EditText tag_name = new EditText(this);
            alert.setView(tag_name);
            alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String val = tag_name.getText().toString();
                    if(img.getTag_list().equals("|")){
                        img.setTag_list(val + "|");
                    }
                    else {
                        String[] result = img.getTag_list().split("\\|");
                        result[0] += ("," + val);
                        String str = result[0] + "|";
                        img.setTag_list(str);
                    }
                    manager.updateData(img);
                    manager.insertTag_byImg_id(img.getImg_id(), val);
                    onResume();
                }
            });
            alert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        switch(v.getId()){
            default:
                break;
        }
    }

    public String makeJSON(){

        JSONObject jsonObject = new JSONObject();

        Unique unique = new Unique();
        jsonObject.put("app_id", unique.getUniqueID(mContext));
        jsonObject.put("image_name", img.getImg_name());
        jsonObject.put("hash_id", img.getHash_id());
        jsonObject.put("tag_list", img.getTag_list());
        jsonObject.put("file", img.getFileData());

        return jsonObject.toString();
    }


    public String POST(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url);

            String json = makeJSON();
            System.out.println("JSON send meg is : " + json);

            StringEntity se = new StringEntity(json);

            httpPost.setEntity(se);

            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();

            if(inputStream != null) {
                result = convertInputStreamToString(inputStream);
            }
            else {
                result = "Did not work!";
                return result;
            }

            System.out.println("Result = " + result);
            if(result.indexOf("Internal") != -1){
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() { // 확인 Alert 창을 위한 Handler 실행
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(DetailViewActivity.this);
                        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.setMessage("Internal Server Error!!");
                        alert.show();
                    }
                }, 0);
                return result;
            }

            JSONParser jsonParser = new JSONParser();
            JSONObject receiveJson = (JSONObject) jsonParser.parse(result);

            final String error_msg = (String)receiveJson.get("error_msg"); // ERROR_MSG를 이용해 성공, 실패 여부 확인

            if(error_msg.equals("success")){ // 정상적으로 햇을 때 success
                System.out.println("good!!");
                String recv_tag_list = (String)receiveJson.get("tag_list");
                img.setTag_list(recv_tag_list);
                img.set_array_tag_list(img.getTag_list());
                manager.insertTag(img);
                System.out.println("tag_list : " + recv_tag_list);
                manager.updateData(img);

                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() { // 확인 Alert 창을 위한 Handler 실행
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(DetailViewActivity.this);
                        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.setMessage("TAG LIST : " + img.getTag_list());
                        alert.show();
                    }
                }, 0);

            }

            else{ // 에러메시지 띄움(원인)
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() { // 확인 Alert 창을 위한 Handler 실행
                    @Override
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(DetailViewActivity.this);
                        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.setMessage("Send & Receive Fail!! : " + error_msg);
                        img.setTag_list("|");
                        manager.updateData(img);
                        alert.show();
                    }
                }, 0);
            }
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
    public Bitmap resizeIMG(String IMG_PATH){
        BitmapFactory.Options bfo = new BitmapFactory.Options();

        /** 보내는 이미지(resize) 객체 생성*/
        if(bm.getHeight() > 4096 || bm.getWidth() > 4096) {
            bfo.inSampleSize = 16;
            sendIMG = BitmapFactory.decodeFile(IMG_PATH, bfo);
        }
        else if(bm.getHeight() > 2048 || bm.getWidth() > 2048){
            bfo.inSampleSize = 8;
            sendIMG = BitmapFactory.decodeFile(IMG_PATH, bfo);
        }
        else if(bm.getHeight() > 1024 || bm.getWidth() > 1024){
            bfo.inSampleSize = 4;
            sendIMG = BitmapFactory.decodeFile(IMG_PATH, bfo);
        }
        else{
            sendIMG = BitmapFactory.decodeFile(IMG_PATH, bfo);
        }
        return sendIMG;
    }


    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return POST(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        //    Toast.makeText(getBaseContext(), "태그 값 Response 완료", Toast.LENGTH_LONG).show();
            onResume();
        }
    }
    class DispClass extends ArrayAdapter<String>{
        Context context;
        public DispClass(Context context, int resource, String[] items) {
            super(context, resource, items);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView label = (TextView)convertView;
            if(convertView == null){
                convertView = new TextView(context);
                label = (TextView)convertView;
            }

            label.setText(view_tag_list[position]);
            label.setTextSize(18);
            label.setGravity(Gravity.CENTER);
            label.setTextColor(Color.argb(255, 255, 255, 255));

            return convertView;
        }
    }
}