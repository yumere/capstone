package example.hahahia.gallery;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import Database.DBManager;
import IMG.IMG;


public class MainActivity extends ActionBarActivity {
    ArrayList<Integer> imgIndexList; // 인덱싱을 위한 ArrayList
    ArrayList<IMG> imgList; // 디비에 있는 이미지 데이터를 가지고 있는 ArrayList
    String search_tag_name = "";
    BroadcastReceiver recv;

    public void setting() {
        setContentView(R.layout.activity_main);
        imgIndexList = new ArrayList<Integer>();
        imgList = new ArrayList<IMG>();
        DBManager manager = new DBManager(getApplicationContext());
        GridView gridview = (GridView) findViewById(R.id.gridview);
        myImageAdapter = new ImageAdapter(this);
        gridview.setAdapter(myImageAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener(){ // 한번 클릭한 경우
            public void onItemClick(AdapterView parent, View v, int position, long id){
                myImageAdapter.callImageViewer(position);
            }
        });
        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // 길게 눌렀을 경우
            public boolean onItemLongClick(AdapterView parent, View v, int position, long id){
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.setMessage("click : " + position);
                alert.show();

                return true;
            }
        });

        /* DB에 있는 이미지들을 가져옴 */
        if(search_tag_name == "") {
            imgList = manager.selectAll();
        }
        else{
            imgList = manager.select_by_tag_name(search_tag_name);
        }

        for(int i=0; i<imgList.size(); i++){
            myImageAdapter.add(imgList.get(i).getImg_path());
            imgIndexList.add(imgList.get(i).getImg_id());
        }

        // Get memory class of this device, exceeding this amount will throw an
        // OutOfMemory exception.

        final int memClass
                = ((ActivityManager)getSystemService(Context.ACTIVITY_SERVICE))
                .getMemoryClass();

        final int cacheSize = 1024 * 1024 * memClass;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {

                return bitmap.getByteCount();
            }

            @Override
            protected void entryRemoved(boolean evicted, String key, Bitmap oldBitmap, Bitmap newBitmap) {
                oldBitmap.recycle();
                oldBitmap = null;
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recv = new NewPhotoReceiver();
        setting();
    }

    @Override
    public void onResume(){
        super.onResume();
        setting();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_search_tag){
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setMessage("검색할 태그명을 입력하세요\n" + "검색 포맷 : (태그명1 태그명2 태그명3 ...)");
            final EditText tag_name = new EditText(this);
            alert.setView(tag_name);
            alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    search_tag_name = tag_name.getText().toString();
                    if(search_tag_name.trim().length() == 0){ // 입력 안 받았을 때 전체화면
                        search_tag_name = "";
                    }
                    onResume();
                }
            });
            alert.show();
        }

        else if(id == R.id.action_search_all){
            search_tag_name = "";
            onResume();
        }

        return super.onOptionsItemSelected(item);
    }


    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        ArrayList<String> itemList = new ArrayList<String>();

        public ImageAdapter(Context c) {
            mContext = c;
        }

        /* 특정 이미지 선택 시 상세보기 페이지로 이동 */
        public final void callImageViewer(int selectedIndex){

            Intent i = new Intent(mContext, DetailViewActivity.class);
            i.putExtra("img_id", imgIndexList.get(selectedIndex));
            startActivityForResult(i, 1);
        }

        void add(String path){
            itemList.add(path);
        }

        @Override
        public int getCount() {
            return itemList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(350, 300));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(2,2,2,2);
            } else {
                imageView = (ImageView) convertView;
            }
            // Use the path as the key to LruCache
            final String imageKey = itemList.get(position);
            final Bitmap bm = getBitmapFromMemCache(imageKey);

            if (bm == null){
                BitmapWorkerTask task = new BitmapWorkerTask(imageView);
                task.execute(imageKey);
            };

            imageView.setImageBitmap(bm);
            return imageView;
        }

        public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {

            Bitmap bm = null;
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(path, options);

            return bm;
        }

        /* 썸네일 사진 설정 */
        public int calculateInSampleSize(

                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 16;

            if (height > reqHeight || width > reqWidth) {
                if (width > height) {
                    inSampleSize = Math.round((float)height / (float)reqHeight);
                } else {
                    inSampleSize = Math.round((float)width / (float)reqWidth);
                }
            }

            return inSampleSize;
        }

        class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap>{

            private final WeakReference<ImageView> imageViewReference;

            public BitmapWorkerTask(ImageView imageView) {
                // Use a WeakReference to ensure the ImageView can be garbage collected
                imageViewReference = new WeakReference<ImageView>(imageView);
            }

            @Override
            protected Bitmap doInBackground(String... params) {
                final Bitmap bitmap = decodeSampledBitmapFromUri(params[0], 200, 200);
                addBitmapToMemoryCache(String.valueOf(params[0]), bitmap);
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (imageViewReference != null && bitmap != null) {
                    final ImageView imageView = (ImageView)imageViewReference.get();
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                }
            }
        }

    }
    ImageAdapter myImageAdapter;
    private LruCache<String, Bitmap> mMemoryCache; // 이미지 cache를 위한 LruCache 세팅

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return (Bitmap) mMemoryCache.get(key);
    }
}