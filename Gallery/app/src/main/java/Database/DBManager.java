package Database;

/**
 * Created by hahahia on 2015-04-19.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import IMG.IMG;
import TAG.TAG;


// DB 총 관리 클래스(CRUD까지)
public class DBManager {

    // DB관련 상수 선언
    private static final String dbName = "Capstone_Gallery.db";
    private static final String tableName_IMG = "IMG_INFO";
    private static final String tableName_TAG = "TAG_INFO";
    public static final int dbVersion = 1;

    // DB관련 객체 선언
    private OpenHelper opener; // DB opener
    private SQLiteDatabase db; // DB controller

    // 부가적인 객체들
    private Context context;

    // 생성자
    public DBManager(Context context) {
        this.context = context;
        this.opener = new OpenHelper(context,  "/mnt/sdcard/" + dbName, null, dbVersion);
        db = opener.getWritableDatabase();
    }

    // Opener of DB and Table
    private class OpenHelper extends SQLiteOpenHelper {

        public OpenHelper(Context context, String name, CursorFactory factory,
                          int version) {
            super(context, name, null, version);
            // TODO Auto-generated constructor stub
        }

        // 생성된 DB가 없을 경우에 한번만 호출됨
        @Override
        public void onCreate(SQLiteDatabase db) {

            String createSql_IMG = "create table " + tableName_IMG + " ("
                    + "img_id integer primary key autoincrement, " + "img_name text, "
                    + "img_path text, " + "tag_list text, "
                    + "created_at DATE, updated_at DATE)";
            db.execSQL(createSql_IMG);

            /* TAG 테이블 생성 */
            String createSql_TAG = "create table " + tableName_TAG + " ("
                    + "tag_id integer primary key autoincrement, " + "img_id integer, "
                    + "tag_name text, "
                    + "created_at DATE, updated_at DATE, UNIQUE(img_id, tag_name) ON CONFLICT REPLACE);";
            db.execSQL(createSql_TAG);


        /* SD카드에서 이미지를 불러오는 루틴 */

            String imageDataPath = null;
            String[] proj = {MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATE_TAKEN,
                    MediaStore.Images.Media.SIZE};
            Cursor imageCursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    proj, null, null, null);
            Calendar cal = new GregorianCalendar();

            if (imageCursor != null && imageCursor.moveToFirst()) {
                String thumbsImageID;
                int thumbsImageIDCol = imageCursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
                do {
                    thumbsImageID = imageCursor.getString(thumbsImageIDCol);
                    if (thumbsImageID != null) {

                        int imgData = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);
                        imageDataPath = imageCursor.getString(imgData);
                        String filename = new File(imageDataPath).getName();
                        /* 앱 최초 실행 후 SD카드에 있는 이미지들을 테이블에 저장함 */
                        String sql = "INSERT INTO " + tableName_IMG + "(img_id, img_name, img_path, tag_list, " +
                                "created_at, updated_at) VALUES(null, '" + filename +
                                        "', '" + imageDataPath + "', null, null, null)";
                        db.execSQL(sql);
                    }
                } while (imageCursor.moveToNext());
            }

            imageCursor.close();

            Toast.makeText(context, "초기 데이터베이스 생성 및 테이블 생성 완료!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
            // TODO Auto-generated method stub
        }
    }

    // 데이터 추가
    public void insertData(IMG img) {

        String sql = "INSERT INTO " + tableName_IMG + "(img_id, img_name, img_path, tag_list) VALUES(null, '" + img.getImg_name() +
                "', '" + img.getImg_path() +
                "', null)";

        System.out.println("sql : " + sql);
        db.execSQL(sql);
    }


    // 데이터 갱신
    public void updateData(IMG img) {

        String sql = "update " + tableName_IMG + " set img_name = '" + img.getImg_name()
                + "', img_path = '" + img.getImg_path()
                + "', tag_list = '" + img.getTag_list()
                + "', updated_at = datetime('now','+9 hours') where img_id = " + img.getImg_id()
                + ";";
        db.execSQL(sql);
    }

    // 데이터 삭제
    public void removeData(IMG img) {
        String delete_Tag = "delete from " + tableName_TAG + " where img_id = " + img.getImg_id() + ";";
        db.execSQL(delete_Tag);
        String delete_IMG = "delete from " + tableName_IMG + " where img_id = " + img.getImg_id() + ";";
        db.execSQL(delete_IMG);
        File file = new File(img.getImg_path());
        file.delete(); // 파일 삭제
    }

    // 데이터 검색
    public IMG selectData(int index) {

        IMG img = new IMG();
        String sql = "select * from " + tableName_IMG + " where img_id = " + index
                + ";";
        Cursor result = db.rawQuery(sql, null);

        // result(Cursor 객체)가 비어 있으면 false 리턴
        if (result.moveToFirst()) {
            img.setImg_id(result.getInt(result.getColumnIndex("img_id")));
            img.setImg_name(result.getString(result.getColumnIndex("img_name")));
            img.setImg_path(result.getString(result.getColumnIndex("img_path")));
            img.setTag_list(result.getString(result.getColumnIndex("tag_list")));
            result.close();
        }
        result.close();
        return img;
    }

    // 이미지 데이터 전체를 나타내는 ArrayList
    public ArrayList<IMG> selectAll() {
        ArrayList<IMG> imgList = new ArrayList<IMG>();
        String sql = "select * from " + tableName_IMG + ";";

        Cursor results = db.rawQuery(sql, null);

        results.moveToFirst();

        while (!results.isAfterLast()) {
            IMG img = new IMG();
            img.setImg_id(results.getInt(results.getColumnIndex("img_id")));
            img.setImg_name(results.getString(results.getColumnIndex("img_name")));
            img.setImg_path(results.getString(results.getColumnIndex("img_path")));
            img.setTag_list(results.getString(results.getColumnIndex("tag_list")));
            imgList.add(img);
            results.moveToNext();
        }
        results.close();
        return imgList;
    }

    // 태그명 기반 검색
    public ArrayList<IMG> select_by_tag_name(String tag_name) {
        ArrayList<IMG> imgList = new ArrayList<IMG>();

        ArrayList<String> array_tag_list = new ArrayList<String>();
        String[] save_tag_list = tag_name.split(" ");

        for(int i=0;i<save_tag_list.length; i++){
            array_tag_list.add(save_tag_list[i]);
        }

        for(int i=0; i<array_tag_list.size(); i++){
            System.out.println("tag name : " + array_tag_list.get(i));
        }

        String sql = "select img_id from (select img_id, group_concat(tag_name) as i from TAG_INFO group by img_id) where ";

        for(int i=0; i<array_tag_list.size(); i++){
            String str = "i like '%" +  array_tag_list.get(i) + "%'";
            if(i != (array_tag_list.size()-1))
                str += " and ";
            sql += str;
        }
        Cursor results = db.rawQuery(sql, null);
        results.moveToFirst();

        while (!results.isAfterLast()) {
            IMG img = this.selectData(results.getInt(results.getColumnIndex("img_id")));
            imgList.add(img);
            results.moveToNext();
        }
        results.close();
        return imgList;
    }

    // 태그 추가
    public void insertTag(IMG img){
        ArrayList<String> tag_list = img.getArray_tag_list();
        for(int i=0; i<tag_list.size(); i++) {

            String sql = "INSERT OR IGNORE INTO " + tableName_TAG + "(tag_id, img_id, tag_name, created_at, updated_at) VALUES(null, '" + img.getImg_id() +
                    "', '" + tag_list.get(i) +
                    "', datetime('now','+9 hours'), null)";

            System.out.println("sql : " + sql);
            db.execSQL(sql);
        }
    }

    // 사용자 직접 태그 추가
    public void insertTag_byImg_id(int img_id, String tag_name){
        String sql = "INSERT OR IGNORE INTO " + tableName_TAG + "(tag_id, img_id, tag_name, created_at, updated_at) VALUES(null, '" + img_id +
             "', '" + tag_name +
             "', datetime('now','+9 hours'), null)";
        db.execSQL(sql);
    }

    // 사용자 직접 태그 삭제
    public void deleteTag_byImg_id(int tag_id){
        String sql = "delete from " + tableName_TAG + " where tag_id = " + tag_id + ";";
        System.out.println(sql);
        db.execSQL(sql);
    }


    /* 해당 이미지의 태그들을 ArryList형태로 리턴 */
    public ArrayList<TAG> selectTagList(IMG img){
        ArrayList<TAG> tag_list = new ArrayList<>();
        String sql = "select * from " + tableName_TAG + " where img_id = " + img.getImg_id() +
                ";";
        Cursor results = db.rawQuery(sql, null);
        results.moveToFirst();
        while (!results.isAfterLast()) {
            TAG tag = new TAG();
            tag.setTag_id(results.getInt(results.getColumnIndex("tag_id")));
            tag.setImg_id(results.getInt(results.getColumnIndex("img_id")));
            tag.setTag_name(results.getString(results.getColumnIndex("tag_name")));
            tag_list.add(tag);
            results.moveToNext();
        }
        results.close();
        return tag_list;
    }


}