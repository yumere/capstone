package IMG;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Created by hahahia on 2015-04-17.
 */

public class IMG {
    private int img_id;
    private String img_name;
    private String img_path;
    private String tag_list;
    private String fileData;
    private String hash_id;
    private ArrayList<String> array_tag_list;

    public void setImg_id(int img_id){
        this.img_id = img_id;
    }
    public void setImg_name(String img_name){
        this.img_name = img_name;
    }
    public void setImg_path(String img_path) { this.img_path = img_path; }
    public void setTag_list(String tag_list){
        this.tag_list = tag_list;
    }
    public void setFileData(String fileData){
        this.fileData = fileData;
    }
    public void setHash_id(String notHashValue){
        this.hash_id = computeMD5Hash(notHashValue);
    }

    public String getImg_name() { return img_name;}
    public String getImg_path() { return img_path; }
    public String getTag_list(){
        return tag_list;
    }
    public String getFileData(){
        return fileData;
    }
    public String getHash_id(){
        return hash_id;
    }
    public int getImg_id(){ return img_id; }
    public ArrayList<String> getArray_tag_list(){ return array_tag_list; }

    public String computeMD5Hash(String str) { // Make Hash
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(str.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer MD5Hash = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                MD5Hash.append(h);
            }
            return MD5Hash.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void set_array_tag_list(String tag_list){
        array_tag_list = new ArrayList<>();
        String[] result = tag_list.split("\\|");
        String result_tag_list = result[0];
        String[] save_tag_list = result_tag_list.split(",");
        for(int i=0;i<save_tag_list.length; i++){
            array_tag_list.add(save_tag_list[i]);
        }
    }
}